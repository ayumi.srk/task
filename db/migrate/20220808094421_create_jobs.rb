class CreateJobs < ActiveRecord::Migration[7.0]
  def change
    create_table :jobs do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.text :name, null: false
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
